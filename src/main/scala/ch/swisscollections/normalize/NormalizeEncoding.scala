/*
 * normalize-encoding
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.normalize

import java.text.Normalizer
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}



object NormalizeEncoding extends Logging {

  /*
  Ich habe das Error - Handling ein wenig mehr scala like gemacht.
  Können wir gerne mal darüber sprechen was da passiert und wo die Unterschiede zur
  Variante von Lionel liegen - dies war mehr Java like - und wir wollten mit den Unterschieden ja mal aufhören

  Ausserdem war Lionels Variante etwas abentuerlich im Fehlerfall
  case e: Exception => value => value
  Was ist denn die Expression des pattern Matching - eine Function
  value => value
  sollte eigentlich nicht funktionieren....
  */
  val nfc: String => String =
      value => Try {
        Normalizer.normalize(value, Normalizer.Form.NFC)
      } match {
        case Success(normalizedEncoding) => normalizedEncoding
        case Failure(exception) =>
          logger.error(s"error normalizing value ${exception.getMessage}")
          value
      }
}
