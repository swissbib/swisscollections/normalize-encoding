package ch.swisscollections.normalize

object KafkaTopologyUtils {

  /**
   *Check if a record comes from SLSP Alma or helveticat
   *
   * @param oai identifier like (RZS)oai:alma.41SLSP_RZS:9914240917605505
   * @return
   */
  def isFromAlma(identifier:String):Boolean = {
    //we assume all alma slsp identifier starts with (***)oai:alma
    if (identifier.length >= 21 && identifier.substring(4,21) == ")oai:alma.41SLSP_") {
      return true
    } else if (identifier.length >= 27 && identifier.substring(0,27) == "oai:helveticat.nb.admin.ch:") {
      return true
    }
    false
  }

}
