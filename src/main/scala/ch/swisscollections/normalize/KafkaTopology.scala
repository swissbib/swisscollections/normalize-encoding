/*
 * normalize-encoding
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.normalize


import org.apache.kafka.streams.{StreamsBuilder, Topology}
import org.apache.logging.log4j.scala.Logging
import scala.jdk.CollectionConverters._

import java.util.Properties

class   KafkaTopology extends Logging {

  def build(
             topicIn: String,
             topicOut: String,
             reportingTopic: String,
             appSettings: Properties
           ): Topology = {

    val builder = new StreamsBuilder

    val source = {
      if (topicIn.contains(",")) {
        val topicCollection = topicIn.split(",").toList
        builder.stream[String, String](topicCollection.asJava)
      } else {
        builder.stream[String, String](topicIn)
      }

    }
    /*@Silvia
    zwei Dinge mal geändert
    ich vermute, dass der vorhergehende Aufruf funktionierte, bin mir nicht ganz sicher
    (_,v) => NormalizeEncoding.nfc(v)
    ist eine Higher Order Function (HOF)
    public interface ValueMapperWithKey<K, V, VR>
    (In intellij mit ctrl-n nach ValueMapperWithKey - dann siehst Du die ganze Signatur)

    mit dem
    (_,v) => NormalizeEncoding.nfc
    muss der Compiler erkennen (und entscheiden) dass die Funktion nfc (Signatur: String => String)
    einen Parameter vom Typ String benötigt und dafür v verwendet.
    Sollte eigentlich gehen mit der untenangegebenen expliziten Variante machst Du das so wie in Deinen Tests

    (und dort hast Du ja ein korrektes Ergebnis, sagst Du)

    2. Sache siehe die Function
     */
    val almaSlspRecords = source.filter((k,_) => KafkaTopologyUtils.isFromAlma(k))

    almaSlspRecords
      .mapValues( v => NormalizeEncoding.nfc(v))
      //filter is needed to avoid messages larger than the default message size being sent
      //to the kafka topic
      .filter((_,v) => v.getBytes.length <= 990000)
      .to(topicOut)
    builder.build()
  }


}
