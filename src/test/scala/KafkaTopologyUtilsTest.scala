/*
 * normalize-encoding
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import ch.swisscollections.normalize.KafkaTopologyUtils
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers


class KafkaTopologyUtilsTest extends AnyFunSuite with Matchers {

  test("is from Alma") {
    assert(KafkaTopologyUtils.isFromAlma("(RZS)oai:alma.41SLSP_RZS:9914218356005505") == true)
    assert(KafkaTopologyUtils.isFromAlma("oai:helveticat.nb.admin.ch:991017986466003976") == true)
    assert(KafkaTopologyUtils.isFromAlma("oai:ZBcollections:a2a82ad8023c419faf379280eb34fc17") == false)
  }
}
