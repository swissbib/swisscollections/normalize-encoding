/*
 * normalize-encoding
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import ch.swisscollections.normalize.NormalizeEncoding
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import testutils.Helpers._

class UnicodeNormalizeTest extends AnyFunSuite with Matchers {

  private lazy val marcfileKakuzo = loadFile("src/test/resources/991051870929705501.xml")

  test("mixed o with macron") {
    assert(NormalizeEncoding.nfc(marcfileKakuzo) contains "<datafield tag=\"100\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Okakura, Kakuzō</subfield>")
    NormalizeEncoding.nfc(marcfileKakuzo) should include ("<datafield tag=\"600\" ind1=\"1\" ind2=\"7\"><subfield code=\"a\">Okakura, Kakuzō</subfield>")
  }

  test("decomposed o with double accute") {
    NormalizeEncoding.nfc("Hubay, Jenő") should equal ("Hubay, Jenő")
  }

  test("precomposed o with double accute") {
    assert(NormalizeEncoding.nfc("Hubay, Jenő") == "Hubay, Jenő")
  }

  test("remains as is") {
    assert(NormalizeEncoding.nfc("<record><header><identifier>oai:alma.41SLSP_UBS:9967636260105504</identifier><datestamp>2020-12-10T22:25:04Z</datestamp><setSpec>ubs_full_set</setSpec></header><metadata>\n<record xmlns=\"http://www.loc.gov/MARC21/slim\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\" ><leader>01549ncm a2200385 c 4500</leader><controlfield tag=\"001\">9967636260105504</controlfield><controlfield tag=\"005\">20201126032533.0</controlfield><controlfield tag=\"008\">171201t20172017gw ||l   ||||||nn   ger|d</controlfield><datafield tag=\"300\" ind1=\" \" ind2=\" \"><subfield code=\"a\">2 Bände (XII, XXIX S., [19] Bl., 1128 S., [46] Bl.)</subfield><subfield code=\"c\">4°</subfield></datafield>") == "<record><header><identifier>oai:alma.41SLSP_UBS:9967636260105504</identifier><datestamp>2020-12-10T22:25:04Z</datestamp><setSpec>ubs_full_set</setSpec></header><metadata>\n<record xmlns=\"http://www.loc.gov/MARC21/slim\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd\" ><leader>01549ncm a2200385 c 4500</leader><controlfield tag=\"001\">9967636260105504</controlfield><controlfield tag=\"005\">20201126032533.0</controlfield><controlfield tag=\"008\">171201t20172017gw ||l   ||||||nn   ger|d</controlfield><datafield tag=\"300\" ind1=\" \" ind2=\" \"><subfield code=\"a\">2 Bände (XII, XXIX S., [19] Bl., 1128 S., [46] Bl.)</subfield><subfield code=\"c\">4°</subfield></datafield>")
  }

  test("decomposed u with diaeresis") {
    assert(NormalizeEncoding.nfc("<datafield tag=\"700\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Brandmüller, Johann Ludwig</subfield><subfield code=\"d\">1680-1751</subfield><subfield code=\"0\">(DE-588)1037506405</subfield></datafield>") == "<datafield tag=\"700\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Brandmüller, Johann Ludwig</subfield><subfield code=\"d\">1680-1751</subfield><subfield code=\"0\">(DE-588)1037506405</subfield></datafield>")
  }

  test("precomposed u with diaeresis") {
    assert(NormalizeEncoding.nfc("<datafield tag=\"700\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Brandmüller, Johann Ludwig</subfield><subfield code=\"d\">1680-1751</subfield><subfield code=\"0\">(DE-588)1037506405</subfield><subfield code=\"e\">Drucker</subfield><subfield code=\"4\">prt</subfield></datafield>") == "<datafield tag=\"700\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Brandmüller, Johann Ludwig</subfield><subfield code=\"d\">1680-1751</subfield><subfield code=\"0\">(DE-588)1037506405</subfield><subfield code=\"e\">Drucker</subfield><subfield code=\"4\">prt</subfield></datafield>")
  }

  test("decomposed o with macron") {
    assert(NormalizeEncoding.nfc("<datafield tag=\"100\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Okakura, Kakuzō</subfield><subfield code=\"d\">1862-1913</subfield><subfield code=\"0\">(DE-588)115571566</subfield></datafield>") == "<datafield tag=\"100\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Okakura, Kakuzō</subfield><subfield code=\"d\">1862-1913</subfield><subfield code=\"0\">(DE-588)115571566</subfield></datafield>")
  }

  test("precomposed o with macron") {
    assert(NormalizeEncoding.nfc("<datafield tag=\"600\" ind1=\"1\" ind2=\"7\"><subfield code=\"a\">Okakura, Kakuzō</subfield><subfield code=\"d\">1862-1913</subfield><subfield code=\"t\">&lt;&lt;The&gt;&gt; book of tea</subfield><subfield code=\"0\">(DE-588)1113598425</subfield><subfield code=\"2\">gnd</subfield></datafield>") == "<datafield tag=\"600\" ind1=\"1\" ind2=\"7\"><subfield code=\"a\">Okakura, Kakuzō</subfield><subfield code=\"d\">1862-1913</subfield><subfield code=\"t\">&lt;&lt;The&gt;&gt; book of tea</subfield><subfield code=\"0\">(DE-588)1113598425</subfield><subfield code=\"2\">gnd</subfield></datafield>")
  }

  test("mixed a with diaeresis") {
    assert(NormalizeEncoding.nfc("<datafield tag=\"020\" ind1=\" \" ind2=\" \"><subfield code=\"a\">9783412508951</subfield><subfield code=\"q\">keine Bindung in Behältnis : circa EUR 24.00 (DE), circa EUR 25.00 (AT)</subfield></datafield><datafield tag=\"255\" ind1=\" \" ind2=\" \"><subfield code=\"a\">Unterschiedliche Massstäbe</subfield><subfield code=\"c\">E 6°22'-E 6°23'/N 51°19'-N 51°18'</subfield></datafield><datafield tag=\"300\" ind1=\" \" ind2=\" \"><subfield code=\"a\">1 Atlas (Textheft 24 Seiten), 1 ungezähltes Blatt, 8 Tafeln</subfield><subfield code=\"b\">Illustrationen, Karten</subfield><subfield code=\"c\">40 x 28 cm</subfield></datafield><datafield tag=\"490\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Rheinischer Städteatlas</subfield><subfield code=\"v\">Nr. 104, Lieferung 21</subfield></datafield><datafield tag=\"830\" ind1=\" \" ind2=\"0\"><subfield code=\"a\">Rheinischer Städteatlas</subfield><subfield code=\"v\">Nr. 104, Lfg. 21</subfield><subfield code=\"w\">991049013029705501</subfield></datafield>") == "<datafield tag=\"020\" ind1=\" \" ind2=\" \"><subfield code=\"a\">9783412508951</subfield><subfield code=\"q\">keine Bindung in Behältnis : circa EUR 24.00 (DE), circa EUR 25.00 (AT)</subfield></datafield><datafield tag=\"255\" ind1=\" \" ind2=\" \"><subfield code=\"a\">Unterschiedliche Massstäbe</subfield><subfield code=\"c\">E 6°22'-E 6°23'/N 51°19'-N 51°18'</subfield></datafield><datafield tag=\"300\" ind1=\" \" ind2=\" \"><subfield code=\"a\">1 Atlas (Textheft 24 Seiten), 1 ungezähltes Blatt, 8 Tafeln</subfield><subfield code=\"b\">Illustrationen, Karten</subfield><subfield code=\"c\">40 x 28 cm</subfield></datafield><datafield tag=\"490\" ind1=\"1\" ind2=\" \"><subfield code=\"a\">Rheinischer Städteatlas</subfield><subfield code=\"v\">Nr. 104, Lieferung 21</subfield></datafield><datafield tag=\"830\" ind1=\" \" ind2=\"0\"><subfield code=\"a\">Rheinischer Städteatlas</subfield><subfield code=\"v\">Nr. 104, Lfg. 21</subfield><subfield code=\"w\">991049013029705501</subfield></datafield>")
  }
}
