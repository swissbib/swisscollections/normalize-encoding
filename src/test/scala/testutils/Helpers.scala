/*
 * normalize-encoding
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package testutils

import scala.io.{BufferedSource, Source}
//import scala.util.{Failure, Success, Try}

object Helpers {

  def loadFile(uri: String): String = {

    /*

    @Silvia: habe hier nur schnell etwas ausprobiert.
    Es geht um das Thema automatissches Ressourcehandling (eine Datei, welche
    geöffnet wurde, sollte automatisch wieder geschlossen werden, wenn sie nicht mehr
    gebraucht wird. Sonst bekommt man ein Ressourcenproblem.
    Mir ging durch den Kopf, ob man dies mit functional Errorhandling machen kann
   Try[welcher Typ ist der Result] {
   ...
   } match {
   case Success(value) => mach etwas
   case Failue (exception) => mach was
   }

   mir scheint, damit geht ein automatisches Ressourcenmanagement nicht
   sondern muss den java Stil verwenden (s. u.)
   aber ist jetzt auch nicht so wichtig, ging mir einfach durch den Kopf und kann man
   irgendwann mal anschauen.

    val result: String = Try {
      Source.fromFile(uri)
    } match {
      case Success(value) =>
        val content = value.getLines().mkString("\n")
        value.close()
        content
      case Failure(exception) =>
        println(exception)
        null
    }

    val javaStyleSource: BufferedSource = Source.fromFile(uri)
    val javaStyleResult: String = try {

      javaStyleSource.getLines().mkString("\n")
    } catch {
      case error: Throwable =>
        println(error.getLocalizedMessage)
        throw new Exception(error)
    }
      finally {
      javaStyleSource.close()
    }

    javaStyleResult

*/

    val source: BufferedSource = Source.fromFile(uri)
    val string = source.getLines().mkString("\n")
    source.close
    string
  }

}
